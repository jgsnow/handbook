---
title: "GitLab System Administration Hands-on Lab Overview"
description: "This hands-on guide is designed to walk you through the lab exercises used in the GitLab System Administration course."
---

# GitLab System Administration

## GitLab System Administration Labs

| Lab Name | Lab Link |
|-----------|------------|
| Install GitLab | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab1) |
| Use GitLab administration commands | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab2) |
| Configure GitLab runners |  [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab3) |
| Backup and restore GitLab |[Lab Link](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab4) |
| Implement sign-up restrictions |  [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab5) |
| Manage GitLab logs |  [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab6) |
| Configure instance monitoring | [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab7) |
| Troubleshoot GitLab |  [Lab Link](/handbook/customer-success/professional-services-engineering/education-services/sysadminhandsonlab8) |


## Quick links

Here are some quick links that may be useful when reviewing this hands-on guide.

- [GitLab System Administration course description](https://about.gitlab.com/services/education/admin/)

## Suggestions?

If you’d like to suggest changes to the *GitLab System Administration Hands-on Guide*, please submit them via merge request.


