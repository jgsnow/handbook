---
title: "GitLab Data Processing Addendum and Standard Contractual Clauses"
---

- [GitLab Data Processing Addendum](https://gitlab.com/gitlab-com/legal-and-compliance/-/raw/master/Customer_DPA__01.04.24_.pdf)
- [Exhibit B - Standard Contractual Clauses](https://gitlab.com/gitlab-com/legal-and-compliance/-/raw/master/Exhibit_B_-_Standard_Contractual_Clauses__01.04.24_.pdf)
